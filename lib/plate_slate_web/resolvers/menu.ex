defmodule PlateSlateWeb.Resolvers.Menu do
  alias PlateSlate.{Menu, Repo}

  def menu_items(_, args, _) do
    {:ok, Menu.list_items(args)}
  end

  def items_for_category(category, _, _) do
    results = Ecto.assoc(category, :items) |> Repo.all()
    {:ok, results}
  end

  def search(_, %{matching: term}, _) do
    {:ok, Menu.search(term)}
  end
end
